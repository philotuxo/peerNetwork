# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'receiver.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ImageProcessor(object):
    def setupUi(self, ImageProcessor):
        ImageProcessor.setObjectName(_fromUtf8("ImageProcessor"))
        ImageProcessor.resize(259, 339)
        self.imageView = QtGui.QGraphicsView(ImageProcessor)
        self.imageView.setGeometry(QtCore.QRect(10, 10, 240, 320))
        self.imageView.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.imageView.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.imageView.setObjectName(_fromUtf8("imageView"))

        self.retranslateUi(ImageProcessor)
        QtCore.QMetaObject.connectSlotsByName(ImageProcessor)

    def retranslateUi(self, ImageProcessor):
        ImageProcessor.setWindowTitle(_translate("ImageProcessor", "Image Receiver", None))

